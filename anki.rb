require 'ankirb'

counter = 1
JSON.parse(File.read('ginger_with_freq.json')).each_slice(50).with_index do |slice, index|
  #create a deck
  deck = Anki::Deck.new "Phrasal Verbs (#{counter}-#{counter + slice.length - 1})"
  counter = counter + slice.length

  slice.each do |word, meaning, example, frequency|
    #create a basic card
    card = Anki::Card.new(front: "<b>#{word}</b> <i>#{example}</i> (#{frequency})", back: meaning)

    #add audio to the back
    # card.back << "dexters_lab_omelette.wav"

    # card = Anki::Card.new :front => "{{img}} text after image", :back => "text before image {{img}}"
    # card.front << "front_img.jpg"
    # card.back << "back_img.jpg"

    #add card to the deck
    deck.add_card card

    #add the reverse version of the card
    deck.add_card card.invert
  end

  #export to ~/Anki/Decks/FancyExampleDeck.apkg
  Anki::apkg.export deck, "/home/sasha/Education/Anki"
end

