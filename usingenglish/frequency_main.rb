require 'nokogiri'
require 'pry'
require 'json'
require_relative '../frequency_fetcher/frequency_fetcher'
require 'open-uri'

MAX_FREQUENCY = 8916

def fetch_html(word)
  file_name = "words_htmls/#{word}.html"
  return File.read(file_name) if File.exist?(file_name)
  url_template = 'https://www.usingenglish.com/reference/phrasal-verbs/%s.html'
  result = open(url_template % word).read
  File.write(file_name, result)
  result
end

# words = JSON.parse(File.read('usingenglish_list.json')).map do |word|
#   [word, FrequencyFetcher.for(word)]
# end.sort_by(&:last).reverse
# File.write('usingenglish_sorted_list.json', words.to_json)
verbs = JSON.parse(File.read('usingenglish_sorted_list.json'))

parsed_verbs = verbs.group_by { |v| v.first.split.first }.map do |base, group|
  html = Nokogiri::HTML(fetch_html(base))
  parsed_group = html.css('.card-title').map do |title|
    word = title.text.downcase
    card = title.parent.parent
    meaning, example = card.css('.card-body .lead').map(&:text)
    meaning.gsub!('Meaning: ', '')
    example.gsub!('Example: ', '')
    notes = card.css('.card-footer li').map { |li| li.text.strip }.join(' | ')
    # frequency = FrequencyFetcher.for(word) / MAX_FREQUENCY.to_f
    frequency = (group.find { |v| v.first.downcase == word }.last / MAX_FREQUENCY.to_f).round(3)

    [word, frequency, meaning, example, notes]
  end.sort_by { |v| -v[1] }
  [base.downcase, parsed_group]
end.sort_by { |verb| verb.last.sum { |v| -v[1] } }

File.write('parsed_verbs.json', JSON.pretty_generate(parsed_verbs))
# ginger_with_freq = JSON.parse(File.read('ginger.json')).each_with_object([]) do |(word, meaning, example), obj|
#   puts word
#   obj << [
#     word.strip,
#     meaning.strip,
#     example.strip,
#     frequency(word)
#   ]
# end.sort_by(&:last).reverse
# File.write('ginger_with_freq.json', ginger_with_freq.to_json)
# File.write('words.json', ginger_with_freq.map { |e| "#{e.first} (#{e.last})" })
