require 'json'
require 'erb'

groups = JSON.parse(File.read('parsed_verbs.json')).map do |base, verbs|
  [base, verbs.map { |v| OpenStruct.new(word: v[0], frequency: v[1], meaning: v[2], example: v[3], notes: v[4]) }]
end

File.write('index.html', ERB.new(File.read('index.html.erb'), nil, binding).result)
