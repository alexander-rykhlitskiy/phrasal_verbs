module FrequencyFetcher
  class << self
    def for(word)
      fetcher.for(word)
    end

    private

    def fetcher
      @fetcher ||= FrequencyFetcher::EngNews.new
    end
  end
end

require_relative 'base'
# require_relative 'corpus'
require_relative 'eng_news'
