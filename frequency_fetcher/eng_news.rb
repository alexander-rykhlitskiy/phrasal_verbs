require 'mysql2'

class FrequencyFetcher::EngNews
  DATABASES = %w[
    eng_news-typical_2016_1M
    eng_news_2013_1M
    eng_news_2014_1M
    eng_news_2015_1M
    eng_news_2016_1M
  ]

  def initialize
    @clients = DATABASES.map { |d| Mysql2::Client.new(username: 'root', database: d) }
  end

  def for(word)
    word = word.downcase
    @clients.map { |c| c.query("SELECT freq FROM words where word='#{word}'").first&.dig('freq').to_i }.sum
  end
end
